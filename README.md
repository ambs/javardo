
# Javardo

A language identification tool based on Neural Networks.



## Dependencies

   * jblas (>= 1.2.3)
   * jcommander (>= 1.35)
   * kryo (>= 2.24.0)
      * minlog (>= 1.2)
      * objenesis (>= 2.1)

## Building from source

The source is managed by Maven (command `mvn`).

The jar file can be built with

    $ mvn clean package

This will result in two different jar files being created:

    target/javardo-<<version>>.jar
    target/javardo-<<version>>-jar-with-dependencies.jar

The first one will need the dependencies jar files in the Java Path. The
second includes the dependencies, and therefore can be ran directly.

## Using the fat-jar

The easiest way to use the fat jar is:

    java -cp javardo-...-jar-with-dependencies.jar org.javardo.LanguageIdentifier file.txt

The result will be an ISO 639-2 language code, together with an associated
probability for that result.

It is also possible to obtain the probability for each language in the neural
network using the `-detailed` command line switch:

    java -cp javardo-...-jar-with-dependencies.jar org.javardo.LanguageIdentifier -detailed file.txt

The application allows the analysis of multiple files at once. Just add more than one
filename in the command line tool.

    java -cp javardo-...-jar-with-dependencies.jar org.javardo.LanguageIdentifier file1.txt file2.txt

Finally, note that the text files must be written in UTF-8.
