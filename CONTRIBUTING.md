# How to Contribute to this Project

## Getting Started

You can contribute to the following categories:

1. Issues listed in the Bitbucket issue tracker
2. Feature Ideas - talk to the maintainers to open a new Issue in Bitbucket
3. Documentation

## Primary Workflow

1. Fork this project and pull it to your machine
2. Add this project as an `upstream` remote so you can periodically merge code
3. Create a feature branch
4. Implement your change in the branch
5. Prepare documentation for your change where necessary
6. Before submitting a merge request, make sure your code is merged with upstream/master
7. Before submitting a merge request, your changes must pass all tests
8. Submit your merge request
9. Work with your maintainers to get the changes merged
10. Wait till your code is pulled into this project's master branch
11. Delete you feature branch
12. Merge your master with upstream/master
