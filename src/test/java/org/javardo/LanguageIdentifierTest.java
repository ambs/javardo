package org.javardo;

import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Map;

/**
 * Created by josemarques on 18/08/14.
 */
public class LanguageIdentifierTest {

    @Test
    public void testFiles() throws URISyntaxException, IOException {

        final LanguageIdentifier li = new LanguageIdentifier();

        final File dir = new File("src/test/resources");
        for(File s : dir.listFiles()) {

            final Features features = new Features(s.toURI());
            final Map.Entry<String, Double> me = li.sortClassifications(
                li.classify(features)
            ).get(0);

            if(!s.getName().contains(me.getKey())) {
                System.out.println(String.format("ERROR [%s]: %s -> %f", s.getName(), me.getKey(), me.getValue()));
            } else {
                Assert.assertTrue(s.getName().contains(me.getKey()));
//                System.out.println(String.format("[%s] %s -> %g", s.getName(), me.getKey(), me.getValue()));
            }

        }

    }
}
