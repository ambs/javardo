/**
 * 
 */
package org.javardo;

import java.io.FileNotFoundException;

/**
 * @author ambs
 *
 */
public class Serializer {

	/**
	 * @param args
	 */
	public static void main(String[] args)
	{	
		String thetas   = "/Users/ambs/Natura/svn/Lingua/Identify-NN/P-PAL/3-thetas.dat";
		String features = "/Users/ambs/Natura/svn/Lingua/Identify-NN/P-PAL/3-features.txt";
		String classes  = "/Users/ambs/Natura/svn/Lingua/Identify-NN/P-PAL/classes.txt";
		String kryo     = "/Users/ambs/Natura/svn/Lingua/Identify-NN/P-PAL/neuralnet.kry";
		
		NeuralNetwork nn = new NeuralNetwork(classes, features, thetas);
		try {
			nn.freeze(kryo);
		} catch (FileNotFoundException e) {
			System.err.println("Error saving kryo: " + e.getMessage());
			System.exit(1);
		}
	}	
}
