/**
 * 
 */
package org.javardo;




import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;

import org.jblas.DoubleMatrix;
import org.jblas.MatrixFunctions;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.Output;

/**
 * @author ambs
 *
 */
public class NeuralNetwork {

	private int nrLayers = 0;
	private int layers[] = null;
	private DoubleMatrix thetas[] = null;
	private ArrayList<String> features = null;
	private ArrayList<String> classes  = null;
	
	/**
	 * Saves a neural network for a Kryogen file.
	 * 
	 * @param kry Filename to use.
	 * @throws FileNotFoundException When file can not be created.
	 */
	public void freeze(String kry) throws FileNotFoundException {
		Kryo kryo = new Kryo();
		kryo.register(NeuralNetwork.class);
		
	    Output output = new Output(new FileOutputStream(kry));
		kryo.writeObject(output, this);
		output.close();
	}
	
	/**
	 * Loads a neural network from a Kryogen file.
	 * 
	 * @param  kry   Filenae to use.
	 * @throws FileNotFoundException  Then file is not found.
	 */
    public void thaw(String kry) throws FileNotFoundException {
        thaw(new FileInputStream(kry));
    }

    /**
     * Loads a neural network from a Kryogen inpustream.
     *
     * @param  kry   Filenae to use.
     * @throws FileNotFoundException  Then file is not found.
     */
    public void thaw(InputStream kry) {
        if(kry==null) throw new IllegalArgumentException("Inputstream Neural network is null.");
		Kryo kryo = new Kryo();
		kryo.register(NeuralNetwork.class);
		
		Input input = new Input(kry);
		this.copy(kryo.readObject(input, NeuralNetwork.class));
		input.close();
	}

	/**
	 * Copy nn contents to this object.
	 * 
	 * @param nn
	 */
	private void copy(NeuralNetwork nn) {
		this.nrLayers = nn.nrLayers;
		this.layers   = nn.layers;
		this.thetas   = new DoubleMatrix[nrLayers-1];
		for (int i = 0; i < nrLayers - 1; i++ ) {
			this.thetas[i] = new DoubleMatrix();
			this.thetas[i].copy(nn.thetas[i]);
		}
		this.classes = nn.classes;
		this.features = nn.features;
	}

	/**
	 * Empty constructor is needed by Kryogen. Should not be used directly.
	 */
	public NeuralNetwork() {
	}
	
	/**
	 * Creates a new neural network from a Kryogen file.
	 * 
	 * @param  kry                    Kryogen filename.
	 */
	public NeuralNetwork(InputStream kry) {
		this.thaw(kry);
	}
	
	/**
	 * Creates a neural network given a file with classes names, a file with features and a file with the theta values.
	 * Mainly useful when generating a new classifier.
	 *  
	 * @param classes   file with class names
	 * @param features  file with list of features
	 * @param thetas    file with the theta values
	 */
	public NeuralNetwork(String classes, String features, String thetas) {
		this.classes  = loadListFromTXT(classes);
		
		// Classes are in the form 'int\tchar' want only the char part
		for (int c = 0; c < this.classes.size(); c++) {
			this.classes.set(c, this.classes.get(c).replaceAll("\\d+\\t", ""));
		}
		
		this.features = loadListFromTXT(features);
		this.bootstrap_neural_network(thetas);
	}
	
	public void listClasses() {
		for (String c : this.classes) {
			System.out.println(c);
		}
	}
	
	public void listFeatures() throws UnsupportedEncodingException {
	    PrintStream out = new PrintStream(System.out, true, "UTF-8");
		for (String c : this.features) {
			out.println(c);
		}
	}
	

	private void bootstrap_neural_network(String file) {
		int filledLayers = 0;

		BufferedReader in = null;
		try {
			in = new BufferedReader(new FileReader(file));
		} catch (FileNotFoundException e) {
			System.out.println("Input file not found. Aborting.");
			System.exit(1);
		}

		// Read the number of layers
		this.nrLayers = Integer.parseInt(readLine(in));

		// Read number of elements per layer
		this.layers = new int[nrLayers];
		while (filledLayers < nrLayers) {
			layers[filledLayers++] = Integer.parseInt(readLine(in));
		}

		// Read each theta matrix
		this.thetas = new DoubleMatrix[this.nrLayers - 1];
		for (int k = 0; k < this.nrLayers-1; k++) {
			thetas[k] = new DoubleMatrix(layers[k+1], layers[k]+1);
			for (int c = 0; c < layers[k]+1; c++) {
				for (int l=0; l < layers[k+1]; l++) {
					thetas[k].put(l, c, Double.parseDouble(readLine(in)));
				}
			}
		}
	}
	
	private static String readLine(BufferedReader in) {
		String s = null;
		try {
			while (in.ready()) {
				s = in.readLine();
				if (! s.startsWith("#")) break;
			}
		} catch (IOException e) {
			System.out.println("Error reading input file. Aborting.");
			System.exit(1);
		}
		s = s.replaceAll(" ","");
		return s;
	}
	
	private static ArrayList<String> loadListFromTXT(String file) {
		ArrayList<String> r = new ArrayList<String>();
		BufferedReader in = null;
		try {
			in = new BufferedReader(new InputStreamReader(new FileInputStream(file), "UTF-8"));
		} catch (FileNotFoundException e) {
			System.out.println("Input file not found. Aborting.");
			System.exit(1);
		} catch (UnsupportedEncodingException e) {
			System.out.println("Input file with wrong encoding? Aborting.");
			System.exit(1);
		}
		
		try {
			while(in.ready()) {
				r.add(in.readLine());
			}
			in.close();
		} catch (IOException e) {
			System.out.println("Error reading file. Aborting.");
			System.exit(1);
		}
		return r;
	}

	public HashMap<String, Double> classify(Features features) {
		
		DoubleMatrix x = grep_features(features);
		DoubleMatrix y = evaluate(x);
		
		HashMap<String, Double> result = new HashMap<String, Double>();
		for (int i = 0; i < this.classes.size(); i++) {
			result.put(this.classes.get(i), y.get(i));
		}
		return result;
	}

	private DoubleMatrix evaluate(DoubleMatrix x)
	{
		DoubleMatrix a[] = new DoubleMatrix[this.nrLayers];
		a[0] = x;
		for (int i = 1; i < this.nrLayers; ++i)
		{
			a[i-1] = DoubleMatrix.concatVertically(DoubleMatrix.ones(1), a[i-1]);
			a[i]   = sigmoid( this.thetas[i-1].mmul( a[i-1] ));
		}
		return a[nrLayers - 1];
	}
	
	private static DoubleMatrix sigmoid(DoubleMatrix z)
	{
		return MatrixFunctions.exp(z.negi()).add(1).rdiv(1);
	}
	
	private DoubleMatrix grep_features(Features features) {
		DoubleMatrix x = new DoubleMatrix(this.features.size(), 1);
		
		for (int i = 0; i < this.features.size(); i++) {
			String f = this.features.get(i);
			x.put(i, 0, features.contains(f) ? features.get(f) : 0.0);
		}
		
		return x;
	}
}
