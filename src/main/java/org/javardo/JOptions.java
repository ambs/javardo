package org.javardo;

import java.util.ArrayList;
import java.util.List;

import com.beust.jcommander.Parameter;

public class JOptions {
	  @Parameter
	  private List<String> parameters = new ArrayList<String>();
	  
	  @Parameter(names = "-detailed", description = "Detailed Output")
	  private boolean detailed = false;
	  
	  public List<String> getArgs() {
		  return this.parameters;
	  }
	  
	  public boolean getDetailed() {
		  return this.detailed;
	  }
}
