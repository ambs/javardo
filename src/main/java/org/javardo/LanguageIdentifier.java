/**
 * 
 */
package org.javardo;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.*;

import com.beust.jcommander.JCommander;


/**
 * @author ambs
 *
 */
public class LanguageIdentifier {

    final NeuralNetwork nn;

    public LanguageIdentifier() {
        this(new NeuralNetwork(LanguageIdentifier.class.getResourceAsStream("/data/neuralnet.kry")));
    }

    public LanguageIdentifier(final NeuralNetwork nn) {
        this.nn = nn;
    }

    public Map<String, Double> classify(final Features features) {
        return nn.classify(features);
    }

    public ArrayList<Map.Entry<String, Double>> sortClassifications(final Map<String, Double> cl) {
        final ArrayList<Map.Entry<String, Double>> ls = new ArrayList<Map.Entry<String, Double>>(cl.entrySet());

        // sort the list
        Collections.sort(ls, new Comparator<Map.Entry<String, Double>>() {
            @Override
            public int compare(Map.Entry<String, Double> o1, Map.Entry<String, Double> o2) {
                return o2.getValue().compareTo(o1.getValue());
            }
        });

        return ls;  // .size()>0 ? ls.get(0) : null;
    }

    /**
	 * @param args
	 */
	public static void main(String[] args) throws URISyntaxException {

		JOptions jComm = new JOptions();
		new JCommander(jComm, args);
		
		List<String> files = jComm.getArgs();
		boolean verbose = jComm.getDetailed();
		boolean many    = files.size() > 1;
		
		if (files.size() < 1) {
			System.err.println("No file supplied.");
			System.exit(1);
		}

        final LanguageIdentifier languageIdentifier = new LanguageIdentifier(
            new NeuralNetwork(LanguageIdentifier.class.getResourceAsStream("/data/neuralnet.kry"))
        );

		for (String file: files) {
            if (many) System.out.println("--[" + file + "]--");

			try {

                final Features features = new Features(new URI(file));
                final ArrayList<Map.Entry<String,Double>> classes =
                    languageIdentifier.sortClassifications(
                        languageIdentifier.classify(features)
                    );

                if (verbose) {
                    for (Map.Entry<String,Double> e: classes) {
                        System.out.format("%s => %.2f%%\n", e.getKey(), 100*e.getValue());
                    }
                } else {
                    Map.Entry<String, Double> r = classes.get(0);
                    System.out.format("%s (%.2f%%)\n", r.getKey() , 100*r.getValue() );
                }

			} catch (IOException e) {
				System.err.println("Error processing file: " + e.getMessage());
				System.exit(1);
			}
		}
	}
}
